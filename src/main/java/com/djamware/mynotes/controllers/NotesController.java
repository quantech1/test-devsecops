package com.djamware.mynotes.controllers;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.djamware.mynotes.models.Notes;
import com.djamware.mynotes.models.User;
import com.djamware.mynotes.repositories.NotesRepository;
import com.djamware.mynotes.services.CustomUserDetailsService;

@Controller
public class NotesController {

	@Autowired
	private CustomUserDetailsService userService;

	@Autowired
	private NotesRepository noteRepository;

	@RequestMapping(value = "/notes", method = RequestMethod.GET)
	public ModelAndView notes() {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		modelAndView.addObject("notes", noteRepository.findAll());
		modelAndView.addObject("currentUser", user);
		modelAndView.addObject("fullName", "Welcome " + user.getFullname());
		modelAndView.addObject("adminMessage", "Content Available Only for Users with Admin Role");
		modelAndView.setViewName("notes");
		return modelAndView;
	}

	@RequestMapping(value="/notes/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		modelAndView.addObject("currentUser", user);
		modelAndView.addObject("fullName", "Welcome " + user.getFullname());
		modelAndView.addObject("adminMessage", "Content Available Only for Users with Admin Role");
		modelAndView.setViewName("create");
		return modelAndView;
	}

	@RequestMapping(value="/notes/save", method = RequestMethod.POST)
	public String save(@RequestParam String title, @RequestParam String content) {
		Notes note = new Notes();
		note.setTitle(title);
		note.setContent(content);
		note.setUpdated(new Date());
		noteRepository.save(note);

		return "redirect:/notes/show/" + note.getId();
	}

	@RequestMapping(value="/notes/show/{id}", method = RequestMethod.GET)
	public ModelAndView show(@PathVariable Long id) {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		modelAndView.addObject("currentUser", user);
		modelAndView.addObject("fullName", "Welcome " + user.getFullname());
		modelAndView.addObject("adminMessage", "Content Available Only for Users with Admin Role");
		modelAndView.addObject("note", noteRepository.findById(id).orElse(null));
		modelAndView.setViewName("show");
		return modelAndView;
	}

	@RequestMapping(value="/notes/delete", method = RequestMethod.DELETE)
	public String delete(@RequestParam Long id) {
		Notes note = noteRepository.findById(id).orElse(null);
		noteRepository.delete(note);

		return "redirect:/notes";
	}

	@RequestMapping(value="/notes/edit/{id}", method = RequestMethod.GET)
	public ModelAndView edit(@PathVariable Long id) {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByEmail(auth.getName());
		modelAndView.addObject("currentUser", user);
		modelAndView.addObject("fullName", "Welcome " + user.getFullname());
		modelAndView.addObject("adminMessage", "Content Available Only for Users with Admin Role");
		modelAndView.addObject("note", noteRepository.findById(id).orElse(null));
		modelAndView.setViewName("edit");
		return modelAndView;
	}

	@RequestMapping(value="/notes/update", method = RequestMethod.PUT)
	public String update(@RequestParam Long id, @RequestParam String title, @RequestParam String content) {
		Notes note = noteRepository.findById(id).orElse(null);
		note.setTitle(title);
		note.setContent(content);
		note.setUpdated(new Date());
		noteRepository.save(note);

		return "redirect:/notes/show/" + note.getId();
	}

}
